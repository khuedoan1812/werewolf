const users = []

const addUser = ({id, name, room}) => {
   const numberOfUsersInRoom = users.filter(user => user.room === room).length
   if(numberOfUsersInRoom === 8)
   return { error: 'Room full' }

   const newUser = { id, name, room }
   users.push(newUser)
   return { newUser }
}

const updateUser = ({id, name}) => {
   for (var i in users) {
      if (users[i].id == id) {
        users[i].name = name;
        break;
      }
   }
}

const removeUser = id => {
   const removeIndex = users.findIndex(user => user.id === id)

   if(removeIndex!==-1)
       return users.splice(removeIndex, 1)[0]
}

const getUser = id => {
   return users.find(user => user.id === id)
}

const getUsersInRoom = room => {
   return users.filter(user => user.room === room)
}

module.exports = { addUser, removeUser, getUser, getUsersInRoom, updateUser }