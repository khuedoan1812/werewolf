const express = require('express')
const socketio = require('socket.io')
const http = require('http')
const cors = require('cors')
const { addUser, removeUser, getUser, getUsersInRoom } = require('./users')
const path = require('path')
const users = require('./users')

const PORT = process.env.PORT || 5000

const app = express()
const server = http.createServer(app)
const io = socketio(server)
const playerName = []
app.use(cors())

io.on('connection', socket => {

    socket.on('join', (payload, callback) => {
        let userName
        if(playerName.length === 0){
            userName = ''
        }else{
            userName = playerName[playerName.length-1].name
        }

        const { error, newUser} = addUser({
            id: socket.id,
            name: userName,
            room: payload.room
        })
        playerName.length = 0;
        if(error)
            return callback(error)

        socket.join(newUser.room)

        io.to(newUser.room).emit('roomData', {room: newUser.room, users: getUsersInRoom(newUser.room)})
        console.log(`someone connect ${newUser.name} and id: ${newUser.id} and room: ${newUser.room}`)
        socket.emit('currentUserData', {
            id: newUser.id,
            name: newUser.name,
            room: newUser.room
        })
        io.to(newUser.room).emit('message', {user: 'Sytem', text: `${newUser.name} has join the game`})
        callback()
    })

    socket.on('initGameState', gameState => {
        const user = getUser(socket.id)
        if(user)
            io.to(user.room).emit('initGameState', gameState)
    })

    socket.on('initCardState', cardState => {
        socket.emit('initCardState', cardState)
    })

    socket.on('initPlayerName', name => {
        playerName.push(name)
    })

    socket.on('updateGameState', gameState => {
        const user = getUser(socket.id)
        if(user)
            io.to(user.room).emit('updateGameState', gameState)
        console.log(getUsersInRoom(user.room))
    })

    socket.on('sendMessage', (payload, callback) => {
        const user = getUser(socket.id)
        io.to(user.room).emit('message', {user: user.name, text: payload.message})
        callback()
    })

    socket.on('disconnect', () => {
        const user = removeUser(socket.id)
        if(user){
            io.to(user.room).emit('roomData', {room: user.room, users: getUsersInRoom(user.room)})
            io.to(user.room).emit('message', {user: 'Sytem', text: `${user.name} has left the game`})
        }
           
    })
})

//serve static assets in production
if(process.env.NODE_ENV === 'production') {
	//set static folder
	app.use(express.static('./../client/build'))
	app.get('*', (req, res) => {
		res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
	})
}

server.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`)
})